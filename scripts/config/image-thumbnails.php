<?php 

return [
    "menu_category" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 138,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "menu_category",
        "description" => "",
        "group" => "",
        "format" => "PNG",
        "quality" => 85,
        "highResolution" => 2.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1578941410,
        "creationDate" => 1571078714,
        "id" => "menu_category"
    ],
    "concession" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 50,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "concession",
        "description" => "",
        "group" => "",
        "format" => "PNG",
        "quality" => 85,
        "highResolution" => 2.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1578941406,
        "creationDate" => 1571078872,
        "id" => "concession"
    ],
    "cast" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 50,
                    "height" => 50,
                    "positioning" => "center",
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "cast",
        "description" => "",
        "group" => "",
        "format" => "PNG",
        "quality" => 85,
        "highResolution" => 2.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => TRUE,
        "modificationDate" => 1576111100,
        "creationDate" => 1571078888,
        "id" => "cast"
    ],
    "poster" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 43,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "poster",
        "description" => "",
        "group" => "",
        "format" => "PNG",
        "quality" => 85,
        "highResolution" => 2.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1574795079,
        "creationDate" => 1571865629,
        "id" => "poster"
    ],
    "belt_Estreno" => [
        "items" => [
            [
                "method" => "addOverlayFit",
                "arguments" => [
                    "path" => "web/var/assets/protected/Movies/Belts/Estreno.png",
                    "composite" => "COMPOSITE_DEFAULT"
                ]
            ],
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 43,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "belt_Estreno",
        "description" => "",
        "group" => "",
        "format" => "PNG",
        "quality" => 85,
        "highResolution" => 2.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1584401172,
        "creationDate" => 1584401172,
        "id" => "belt_Estreno"
    ],
    "belt_Preventa" => [
        "items" => [
            [
                "method" => "addOverlayFit",
                "arguments" => [
                    "path" => "web/var/assets/protected/Movies/Belts/Preventa.png",
                    "composite" => "COMPOSITE_DEFAULT"
                ]
            ],
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 43,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "belt_Preventa",
        "description" => "",
        "group" => "",
        "format" => "PNG",
        "quality" => 85,
        "highResolution" => 2.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1576283488,
        "creationDate" => 1576104852,
        "id" => "belt_Preventa"
    ],
    "trailer" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 138,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "trailer",
        "description" => "",
        "group" => "",
        "format" => "PNG",
        "quality" => 85,
        "highResolution" => 2.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1576003428,
        "creationDate" => 1576002425,
        "id" => "trailer"
    ],
    "belt_Garantia" => [
        "items" => [
            [
                "method" => "addOverlayFit",
                "arguments" => [
                    "path" => "web/var/assets/protected/Movies/Belts/Garantia.png",
                    "composite" => "COMPOSITE_DEFAULT"
                ]
            ],
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 43,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "belt_Garantia",
        "description" => "",
        "group" => "",
        "format" => "PNG",
        "quality" => 85,
        "highResolution" => 2.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1576283422,
        "creationDate" => 1576104827,
        "id" => "belt_Garantia"
    ],
    "belt_SalaDeArte" => [
        "items" => [
            [
                "method" => "addOverlayFit",
                "arguments" => [
                    "path" => "web/var/assets/protected/Movies/Belts/SalaDeArte.png",
                    "composite" => "COMPOSITE_DEFAULT"
                ]
            ],
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 43,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "belt_SalaDeArte",
        "description" => "",
        "group" => "",
        "format" => "PNG",
        "quality" => 85,
        "highResolution" => 2.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1576283535,
        "creationDate" => 1576104840,
        "id" => "belt_SalaDeArte"
    ]
];
