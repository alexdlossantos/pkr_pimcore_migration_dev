#!/bin/bash
echo "******************   Environment Variables"
export PHPRC=/etc/php/7.2/apache2/php.ini
export COMPOSER_ALLOW_SUPERUSER=1
export COMPOSER_HOME="/var/www/html/"
export PIMCORE_ENVIRONMENT="dev"
sudo sh -c "echo 'PIMCORE_ENVIRONMENT="dev"' >> /etc/environment"
sudo sh -c "echo 'COMPOSER_HOME="/var/www/html/"' >> /etc/environment"

echo "******************   Installing packages"
sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update -y
sudo apt-get install -y --allow-unauthenticated php7.2
sudo apt-get install -y --allow-unauthenticated php-pear
sudo apt install -y apache2 libapache2-mod-php7.2
sudo apt install -y mysql-client nfs-common

sudo apt-get update
sudo apt-get install -y git binutils make
git clone https://github.com/aws/efs-utils
cd efs-utils
make deb
sudo apt-get install -y ./build/amazon-efs-utils*deb


echo "******************   replace php.ini"
sudo cp /tmp/scripts/config/php.ini /etc/php/7.2/apache2/
sudo cp /tmp/scripts/config/php.ini /etc/php/7.2/cli/php.ini

sudo apt-get install -y --allow-unauthenticated \
    ffmpeg \
    git \
	php7.2-dev \
	php7.2-gd \
	php7.2-xml \
	php7.2-opcache \
	php7.2-imagick \
	php7.2-bcmath \
	php7.2-curl \
	php7.2-intl \
	php7.2-mbstring \
	php7.2-mysqlnd \
	php7.2-redis \
	php7.2-zip \
	php7.2-simplexml \
	libsodium-dev \
	libzip-dev \
	libpng-dev \
	zlib1g-dev \
	libicu-dev \
	libxml2-dev \
	g++ \
	mariadb-client \
	zip \
	unzip \
	graphviz;

pecl install -f libsodium

echo "******************   Configuring apache"
sudo sh -c "echo 'ServerName localhost' > /etc/apache2/conf-available/fqdn.conf"
sudo sh -c "echo 'extension=sodium' > /etc/php/7.2/mods-available/sodium.ini"
sudo cp /tmp/scripts/config/pimcore.conf /etc/apache2/sites-available/pimcore.conf
sudo a2enconf fqdn
sudo phpenmod sodium
sudo a2dissite 000-default
sudo a2ensite pimcore
sudo a2enmod rewrite
sudo systemctl enable apache2



echo "******************   Downloading last version"
sudo rm -rf /var/www/html && \
sudo git clone -b dev https://cinepolisdevops:DhZRkBun2uqeadhqKt5G@bitbucket.org/cinepolisdigital/pim_pimcore_mvp.git /var/www/html && \
sudo git config --global user.email "pimprodcinepolis@softtek.com" && \
sudo git config --global user.name "prod pimcore";

sudo mkdir  /var/www/html/resconfig
sudo cp /var/www/html/var/config/* /var/www/html/resconfig/

echo "******************   Installing Composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

sudo echo "fs-9dca061d.efs.us-east-1.amazonaws.com:/ /var/www/html/var/config efs defaults,_netdev 0 0" >> /etc/fstab
sudo mount -a
sudo chmod -R 755 /var/www/html/var/config
sudo chown -R www-data:www-data /var/www/html/var/config

sudo cp  /var/www/html/resconfig/* /var/www/html/var/config/
sudo rm -rf /var/www/html/resconfig

sudo ls -l /var/www/html/var/config/
sudo cp /tmp/scripts/config/system.php /var/www/html/var/config/system.php
sudo cp /tmp/scripts/config/debug-mode.php /var/www/html/var/config/debug-mode.php
sudo ls -l /var/www/html/var/config/
sudo chmod 755 -R /var/www/html/

echo "******************   Installing Pimcore dependencies"
sudo rm -rf /var/www/html/app/cache/
sudo rm -rf /var/www/html/var/cache/
sudo rm -rf /var/www/html/app/config/local/database.yml
sudo cp /tmp/scripts/config/database.yml /var/www/html/app/config/local/

sudo mv /tmp/scripts/config/constants.php /var/www/html/app
sudo mv /tmp/scripts/config/startup.php /var/www/html/app

sudo cp /tmp/scripts/config/image-thumbnails.php  /var/www/html/var/config/

#download composer deps on parallel
composer global require hirak/prestissimo

cd /var/www/html/ && sudo php -dmemory_limit=4G /usr/local/bin/composer install --no-scripts && \
sudo mv /tmp/scripts/config/config.env /var/www/html/.env
sudo mv /tmp/scripts/config/config.yml /var/www/html/app/config/

##error https://github.com/pimcore/pimcore/issues/1967
cd /var/www/html
sudo php -dmemory_limit=4G /usr/local/bin/composer run-script installSymfonyAssets

sudo chmod 755 -R /var/www/html/
sudo chown -R www-data:www-data /var/www/html/

# sudo mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-74c2bff5.efs.us-east-1.amazonaws.com:/ /var/www/html/var/config/
sudo ls -l /var/www/html/var/config/

sudo service apache2 restart
php -dmemory_limit=4G ./bin/console pimcore:deployment:classes-rebuild --create-classes --delete-classes --no-interaction --verbose
sudo php -dmemory_limit=4G  /var/www/html/bin/console pimcore:deployment:custom-layouts-rebuild


sudo mv /tmp/scripts/config/Asset.php /var/www/html/vendor/pimcore/pimcore/models

echo "******************   Installing RAM Agent"
cd /tmp
sudo apt-get update
sudo apt-get install -y unzip
sudo apt-get install -y libwww-perl libdatetime-perl
curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
unzip CloudWatchMonitoringScripts-1.2.2.zip && \
rm CloudWatchMonitoringScripts-1.2.2.zip && \
cd aws-scripts-mon
cp awscreds.template awscreds.conf
echo '''
AWSAccessKeyId=AKIA2ZVZBFUVRD56VP7B
AWSSecretKey=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
''' >> awscreds.conf
sudo mv ../aws-scripts-mon /opt

sudo crontab -l > /opt/agentcron
sudo echo "*/5 * * * * /opt/aws-scripts-mon/mon-put-instance-data.pl --mem-used --mem-util --mem-avail --swap-util --swap-used --from-cron --auto-scaling" >> /opt/agentcron
sudo crontab /opt/agentcron

echo "**************** cleanup cronjob"
#maintenance
sudo crontab -l > /opt/maintenance
sudo echo "*/5 * * * * www-data /var/www/html/bin/console maintenance" >> /opt/maintenance
sudo crontab /opt/maintenance
#clear cache
sudo crontab -l > /opt/cleancron
sudo echo "0 13 * * * root /var/www/html/bin/console cache:clear --env=dev" >> /opt/cleancron
sudo crontab /opt/cleancron


#-----------DESCOMENTAR CUANDO EXISTA ELK STACK EN PRODUCCION
# echo "**************** Installing Metricbeat"

# curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.3.1-amd64.deb
# sudo dpkg -i metricbeat-7.3.1-amd64.deb

# sudo mv /tmp/metricbeat.yml /etc/metricbeat/metricbeat.yml
# sudo chown root:root /etc/metricbeat/metricbeat.yml
# sudo metricbeat modules enable system

# sudo metricbeat setup
# sudo systemctl enable metricbeat

# echo "**************** Installing Filebeat"

# curl -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.3.1-amd64.deb
# sudo dpkg -i filebeat-7.3.1-amd64.deb

# sudo mv /tmp/filebeat.yml /etc/filebeat/filebeat.yml
# sudo chown root:root /etc/filebeat/filebeat.yml
# sudo filebeat modules enable apache
# sudo mv /tmp/apache.yml /etc/filebeat/modules.d/apache.yml
# sudo chown root:root /etc/filebeat/modules.d/apache.yml

# sudo filebeat setup
# sudo systemctl enable filebeat
sudo ls -l /var/www/html/var/config/